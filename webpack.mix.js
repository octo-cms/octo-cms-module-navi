const dotenvExpand = require('dotenv-expand');
dotenvExpand(require('dotenv').config({path: '../../.env'/*, debug: true*/}));

const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

const assetPath = '../../public/assets_navi';

mix.copyDirectory(__dirname + '/Resources/assets/img', assetPath + '/img');

mix.scripts([
    __dirname + '/Resources/assets/js/core/jquery.min.js',
    __dirname + '/Resources/assets/js/core/popper.min.js',
    __dirname + '/Resources/assets/js/core/bootstrap.min.js',
], assetPath + '/js/all-scripts.min.js');

mix.sass(
    __dirname + '/Resources/assets/scss/paper-kit.scss',
    assetPath + '/css/main.min.css'
)
// .purgeCss({
//
//     enabled: false,
//     content: [
//         path.join(__dirname, '/Resources/views/**/*.php'),
//         path.join(__dirname, '/Resources/views/!**!/!**/!*.php'),
//     ],
//     whitelistPatterns: [/aos/, /flickity/, /bg-/, /text-/, /transform-/, /divider-/,/divider-/,/dropdown(.*)/,/row/,/col-auto/,/fade-page/,/nav(.*)/],
// })

// if (mix.inProduction()) {
    mix.version();
// }
