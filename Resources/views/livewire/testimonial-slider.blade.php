@foreach($this->testimonials as $testimonial)
    <div class="carousel-item {{$loop->first ? 'active' : ''}}">
        <div class="card card-testimonial card-plain">
            <div class="card-avatar">
                <x-image-component :picture="$testimonial['picture']"
                                   :src="asset('assets_navi/img/placeholder.jpg')"
                                   class="img">
                </x-image-component>
            </div>
            <div class="card-body">
                @if(!empty($testimonial['text']))
                    <h5 class="card-description">{{$testimonial['text']}}</h5>
                @endif
                <div class="card-footer">
                    @if(!empty($testimonial['author']))
                        <h4 class="card-title">{{$testimonial['author']}}</h4>
                    @endif
                    @if(!empty($testimonial['job']))
                        <h6 class="card-category">{{$testimonial['job']}}</h6>
                    @endif
                    <div class="card-stars">
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
