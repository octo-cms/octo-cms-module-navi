@foreach($this->news as $entity)
    <div class="col-md-6">
        <div class="card card-plain card-blog text-center">
            <div class="card-image">
                <a href="{{$entity['page']}}">
                    <x-image-component class="img img-raised"
                                       :picture="$entity['picture']"
                                       :src="asset('assets_navi/img/placeholder.jpg')">
                    </x-image-component>
                </a>
            </div>
            <div class="card-body">
                @if(!empty($entity['category']))
                    <h6 class="card-category text-warning">{!! $entity['category'] !!}</h6>
                @endif
                @if(!empty($entity['title']))
                    <h3 class="card-title">
                        <a href="{{$entity['page']}}">{!! $entity['title'] !!}</a>
                    </h3>
                @endif
                @if(!empty($entity['short_description']))
                    <p class="card-description">{!! $entity['short_description'] !!}</p>
                @endif
                <br/>
                <a href="{{$entity['page']}}" class="btn btn-warning btn-round"> Read More</a>
            </div>
        </div>
    </div>
@endforeach
