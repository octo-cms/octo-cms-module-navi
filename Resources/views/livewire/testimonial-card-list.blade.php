@foreach($this->testimonials as $testimonial)
    <div class="col-md-4">
        <div class="card card-testimonial">
            <div class="card-icon">
                <i class="fa fa-quote-right" aria-hidden="true"></i>
            </div>
            <div class="card-body">
                @if(!empty($testimonial['text']))
                    <p class="card-description">{{$testimonial['text']}}</p>
                @endif
                <div class="card-footer">
                    @if(!empty($testimonial['author']))
                        <h4 class="card-title">{{$testimonial['author']}}</h4>
                    @endif
                    @if(!empty($testimonial['job']))
                        <h6 class="card-category">{{$testimonial['job']}}</h6>
                    @endif
                    <div class="card-avatar">
                        <x-image-component :picture="$testimonial['picture']"
                                           :src="asset('assets_navi/img/placeholder.jpg')"
                                           class="img">
                        </x-image-component>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
