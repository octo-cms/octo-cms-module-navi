<div class="col-md-4 offset-1">
    @foreach($this->services as $service)
        <div class="info info-horizontal {{$loop->first ? 'pt-4 pt-md-0' : ''}}">
            <div class="icon icon-info">
                @if(!empty($service['customIcon']))
                    <x-image-component :picture="$service['customIcon']"
                                       :src="asset('assets_navi/img/placeholder.jpg')">
                    </x-image-component>
                @elseif(!empty($service['icon']))
                    <i class="nc-icon nc-{{$service['icon']}}" aria-hidden="true"></i>
                @endif
            </div>
            <div class="description">
                @if(!empty($service['name']))
                    <h4 class="info-title">{!! $service['name'] !!}</h4>
                @endif
                @if(!empty($service['description']))
                    <p>{!! $service['description'] !!}</p>
                @endif
            </div>
        </div>
    @endforeach
</div>
