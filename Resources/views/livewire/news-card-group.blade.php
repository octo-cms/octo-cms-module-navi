@foreach($this->news as $entity)
    <div class="col-md-{{$entity['colSize']}} mb-4">
        <div class="card h-100" data-background="image"
             style="background-image: url({{$this->getImageSrc($entity['picture'])}})">
            <div class="card-body">
                @if(!empty($entity['category']))
                    <h6 class="card-category">
                        <i class="fa fa-newspaper-o"></i> {!! $entity['category'] !!}
                    </h6>
                @endif
                @if(!empty($entity['title']))
                    <a href="{{!empty($entity['page']) ? $this->getPageUrl($entity['page']) : '#'}}">
                        <h3 class="card-title">{!! $entity['title'] !!}</h3>
                    </a>
                @endif
                @if(!empty($entity['short_description']))
                    <p class="card-description">
                        {!! $entity['short_description'] !!}
                    </p>
                @endif
                @if(!empty($entity['author']))
                    <div class="card-footer">
                        <div class="author">
                            {!! $entity['author'] !!}
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endforeach
