@foreach($this->news as $entity)
    <div class="col-md-4">
        <div class="card card-blog">
            <div class="card-image">
                <a href="{{$entity['page']}}">
                    <x-image-component class="img img-raised"
                                       :picture="$entity['picture']"
                                       :src="asset('assets_navi/img/placeholder.jpg')">
                    </x-image-component>
                </a>
            </div>
            <div class="card-body">
                <h6 class="card-category text-info">{!! $entity['category'] !!}</h6>
                <h5 class="card-title">
                    <a href="{{$entity['page']}}">{!! $entity['title'] !!}</a>
                </h5>
                <p class="card-description">
                    {!! $entity['short_description'] !!}<br/>
                </p>
                <hr>
                <div class="card-footer">
                    <div class="author">
                        <span>{!! $entity['author'] !!}</span>
                    </div>
                    <div class="stats">
                        <i class="fa fa-clock-o" aria-hidden="true"></i> {!! $entity['date'] !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
