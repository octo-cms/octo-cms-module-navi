@foreach($this->testimonials as $testimonial)
    <div class="{{$testimonial['colClass']}}">
        <div class="card h-100" data-background="color" data-color="{{$testimonial['color']}}">
            <div class="card-body">
                <div class="author">
                    <a href="#">
                        <x-image-component :picture="$testimonial['picture']"
                                           :src="asset('assets_navi/img/placeholder.jpg')"
                                           class="avatar img-raised">
                        </x-image-component>
                        @if(!empty($testimonial['author']))
                            <span>{{$testimonial['author']}}</span>
                        @endif
                    </a>
                </div>
                <span class="category-social pull-right">
                  <i class="fa fa-quote-right"></i>
                </span>
                <div class="clearfix"></div>
                @if(!empty($testimonial['text']))
                    <p class="card-description">{{$testimonial['text']}}</p>
                @endif
            </div>
        </div>
    </div>
@endforeach
