@foreach($this->news as $entity)
    @if($loop->odd)
        <div class="card card-plain card-blog">
            <div class="row">
                <div class="col-md-5">
                    <div class="card-image d-flex justify-content-center">
                        <x-image-component class="img"
                                           :picture="$entity['picture']"
                                           :src="asset('assets_navi/img/placeholder.jpg')">
                        </x-image-component>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="card-body">
                        @if(!empty($entity['category']))
                            <h6 class="card-category text-info">{!! $entity['category'] !!}</h6>
                        @endif
                        @if(!empty($entity['title']))
                            <h3 class="card-title">
                                <a href="{{$entity['page']}}">
                                    {!! $entity['title'] !!}
                                </a>
                            </h3>
                        @endif
                        @if(!empty($entity['short_description']))
                            <p class="card-description">
                                {!! $entity['short_description'] !!}
                                <a href="{{$entity['page']}}"> Read More </a>
                            </p>
                        @endif
                        @if(!empty($entity['author']))
                            <p class="author">
                                <b>{!! $entity['author'] !!}</b>, {!! $entity['date'] !!}
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="card card-plain card-blog">
            <div class="row">
                <div class="col-md-7">
                    <div class="card-body">
                        @if(!empty($entity['category']))
                            <h6 class="card-category text-info">{!! $entity['category'] !!}</h6>
                        @endif
                        @if(!empty($entity['title']))
                            <h3 class="card-title">
                                <a href="{{$entity['page']}}">{!! $entity['title'] !!}</a>
                            </h3>
                        @endif
                        @if(!empty($entity['short_description']))
                            <p class="card-description">
                                {!! $entity['short_description'] !!}
                                <a href="{{$entity['page']}}"> Read More </a>
                            </p>
                        @endif
                        @if(!empty($entity['author']))
                            <p class="author">
                                <b>{!! $entity['author'] !!}</b>, {!! $entity['date'] !!}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="card-image d-flex justify-content-center">
                        <x-image-component class="img"
                                           :picture="$entity['picture']"
                                           :src="asset('assets_navi/img/placeholder.jpg')">
                        </x-image-component>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endforeach
