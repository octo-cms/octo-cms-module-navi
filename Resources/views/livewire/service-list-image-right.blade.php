@foreach($this->services as $service)
    <div class="col-6">
        <div class="info info-horizontal">
            <div class="icon icon-success">
                @if(!empty($service['customIcon']))
                    <x-image-component :picture="$service['customIcon']"
                                       :src="asset('assets_navi/img/placeholder.jpg')">
                    </x-image-component>
                @elseif(!empty($service['icon']))
                    <i class="nc-icon nc-{{$service['icon']}}"></i>
                @endif
            </div>
            <div class="description">
                @if(!empty($service['name']))
                    <h4 class="info-title">{!! $service['name'] !!}</h4>
                @endif
                @if(!empty($service['description']))
                    <p>{!! $service['description'] !!}</p>
                @endif
                <a href="{{$service['page']}}" class="btn btn-link">See more</a>
            </div>
        </div>
    </div>
@endforeach
