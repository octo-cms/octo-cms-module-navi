@foreach($this->services as $service)
    <div class="col-md-4">
        <div class="card" data-background="image"
             style="background-image: url({{$this->getImageSrc($service['picture'])}})">
            <div class="card-body">
                @if(!empty($service['name']))
                    <h6 class="card-category">{!! $service['name'] !!}</h6>
                @endif
                <div class="card-icon">
                    @if(!empty($service['customIcon']))
                        <x-image-component :picture="$service['customIcon']"
                                           :src="asset('assets_navi/img/placeholder.jpg')">
                        </x-image-component>
                    @elseif(!empty($service['icon']))
                        <i class="nc-icon nc-{{$service['icon']}}"></i>
                    @endif
                </div>
                @if(!empty($service['description']))
                    <p class="card-description">{!! $service['description'] !!}</p>
                @endif
                <div class="card-footer">
                    <a href="{{$service['page']}}"
                       class="btn btn-link btn-neutral">
                        <i class="fa fa-book" aria-hidden="true"></i> Show more
                    </a>
                </div>
            </div>
        </div>
    </div>
@endforeach
