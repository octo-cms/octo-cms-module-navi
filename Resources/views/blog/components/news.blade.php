<div class="page-header" data-parallax="true"
     style="background-image: url({{$getImageSrc(optional($news->getPictures('main')->first())->toArray())}})">
    <div class="filter"></div>
    <div class="content-center">
        <div class="motto container">
            <h1 class="title-uppercase text-center">{{$getLangValue($news->newsLangs, 'title')}}</h1>
            <h3 class="text-center">{{$getLangValue($news->newsLangs, 'short_description')}}</h3>
            <br/>
            <a href="#" class="btn btn-warning btn-round btn-lg">
                <i class="fa fa-share-alt" aria-hidden="true"></i> Share Article
            </a>
        </div>
    </div>
</div>
<div class="section section-white">
    <div class="container">
        <div class="row">
            <div class="col-md-8 ml-auto mr-auto text-center title">
                <h2>{{$getLangValue($news->newsLangs, 'title')}}</h2>
                <h3 class="title-uppercase mt-0"><small>{{$news->author}}</small></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 ml-auto mr-auto mb-4">
                <div class="text-center">
                    @if(!empty($news->categories))
                        <span class="badge badge-warning main-tag mb-3">
                            {{$getLangValue($news->categories->first()->categoryLangs, 'name')}}
                        </span>
                    @endif
                    <h6 class="title-uppercase">{{optional($news->date)->format('d/m/Y')}}</h6>
                </div>
            </div>
            <div class="col-md-8 ml-auto mr-auto">
                @if(!empty($news->newsContents))
                    <div class="article-content">
                        @foreach($news->newsContents as $newsContent)
                            <x-news-content-handler :news-content="$newsContent"/>
                        @endforeach
                    </div>
                @endif
                <br/>
                <div class="article-footer">
                    <div class="container">
                        <div class="row">
                            @if(!empty($news->tags))
                                <div class="col-md-6">
                                    <h5>Tags:</h5>
                                    @foreach($news->tags as $tag)
                                        <span class="label label-default">{{$tag->name}}</span>
                                    @endforeach
                                </div>
                            @endif
                            <div class="col-md-4 ml-auto">
                                <div class="sharing">
                                    <h5>Spread the word</h5>
                                    <button class="btn btn-just-icon btn-twitter">
                                        <i class="fa fa-twitter"></i>
                                    </button>
                                    <button class="btn btn-just-icon btn-facebook">
                                        <i class="fa fa-facebook"> </i>
                                    </button>
                                    <button class="btn btn-just-icon btn-google">
                                        <i class="fa fa-google"> </i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="container">
                    <div class="row">
                        <div class="media">
                            <div class="media-body">
                                <h4 class="media-heading">{{$news->author}}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Related News component -->
        <div class="row">
            <div class="related-articles">
                <h3 class="title">Related articles</h3>
                <legend></legend>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <a href="#"><img src="{{asset('assets_navi/img/sections/damir-bosnjak.jpg')}}"
                                             alt="Rounded Image" class="img-rounded img-responsive"></a>
                            <p class="blog-title">My Review of Pitchfork’s ‘Indie 500’ Album Review</p>
                        </div>
                        <div class="col-md-4">
                            <a href="#"><img src="{{asset('assets_navi/img/sections/por7o.jpg')}}"
                                             alt="Rounded Image"
                                             class="img-rounded img-responsive"></a>
                            <p class="blog-title">Top Events This Month</p>
                        </div>
                        <div class="col-md-4">
                            <a href="#"><img src="{{asset('assets_navi/img/sections/jeff-sheldon.jpg')}}"
                                             alt="Rounded Image"
                                             class="img-rounded img-responsive"></a>
                            <p class="blog-title">You Should Get Excited About Virtual Reality. Here’s Why.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Related News component END -->
    </div>
</div>

