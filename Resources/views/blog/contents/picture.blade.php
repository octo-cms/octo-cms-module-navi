<div class="row">
    <div class="col-md-12">
        <div class="card" data-radius="none">
            <x-image-component :picture="$picture->toArray()"
                               :src="asset('assets_navi/img/placeholder.jpg')">
            </x-image-component>
        </div>
    </div>
</div>

