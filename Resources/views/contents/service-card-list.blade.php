<div class="features-2">
    <div class="container">
        <div class="row">
            <div class="col-md-8 ml-auto mr-auto text-center">
                @if($data['title'])
                    <h2 class="title">{{$data['title']}}</h2>
                @endif
                @if(!empty($data['description']))
                    <h5 class="description">{{$data['description']}}</h5>
                @endif
                <br/>
            </div>
        </div>
        <div class="row">
            @livewire('service-card-list', ['targets' => $targets])
        </div>
    </div>
</div>
