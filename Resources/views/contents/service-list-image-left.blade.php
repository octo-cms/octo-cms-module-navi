<div class="features-4">
    <div class="container">
        <div class="row">
            <div class="col-md-8 ml-auto mr-auto text-center">
                @if($data['title'])
                    <h2 class="title">{{$data['title']}}</h2>
                @endif
                @if(!empty($data['description']))
                    <h5 class="description">{{$data['description']}}</h5>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 d-flex justify-content-center">
                <div class="ipad-container">
                    <x-image-component :picture="$data['image']"
                                       :src="asset('assets_navi/img/placeholder.jpg')">
                    </x-image-component>
                </div>
            </div>
            @livewire('service-list-image-left', ['targets' => $targets])
        </div>
    </div>
</div>
