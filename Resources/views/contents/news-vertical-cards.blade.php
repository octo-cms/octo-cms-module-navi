<div class="blog-2 section section-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-10 ml-auto mr-auto">
                @if(!empty($data['title']))
                    <h2 class="title">{{$data['title']}}</h2>
                @endif
                <br/>
                <div class="row">
                    @livewire('news-vertical-cards', ['targets' => $targets])
                </div>
            </div>
        </div>
    </div>
</div>
