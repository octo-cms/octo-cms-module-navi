<div class="testimonials-1 section-image" style="background-image: url({{!empty($data['image'])
                    ? (env('MIX_BUCKET_BASE_URL') .'/'. env('MIX_BUCKET_NAME') .'/'. $data['image']['src'])
                    : asset('assets_navi/img/image_placeholder.jpg')}})">
    <div class="container">
        <div class="row">
            <div class="col-md-6 ml-auto mr-auto text-center">
                @if(!empty($data['title']))
                    <h2 class="title">{{$data['title']}}</h2>
                @endif
                @if(!empty($data['description']))
                    <h5 class="description">{{$data['description']}}</h5>
                @endif
            </div>
        </div>
        <div class="space-top"></div>
        <div class="row">
            @livewire('testimonial-card-list', ['targets' => $targets])
        </div>
    </div>
</div>
