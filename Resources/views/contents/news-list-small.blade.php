<div class="blog-3">
    <div class="container">
        <div class="row">
            <div class="col-md-10 ml-auto mr-auto">
                @if(!empty($data['title']))
                    <h2 class="title">{!! $data['title'] !!}</h2>
                @endif
                <br/>
                @livewire('news-list-small', ['targets' => $targets])
            </div>
        </div>
    </div>
</div>
