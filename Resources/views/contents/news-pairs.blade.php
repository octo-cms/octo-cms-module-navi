<div class="blog-4">
    <div class="container">
        @if(!empty($data['title']))
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto">
                    <h2 class="title text-center">{!! $data['title'] !!}</h2>
                    <br/>
                </div>
            </div>
        @endif
        <div class="row">
            @livewire('news-pairs', ['targets' => $targets])
        </div>
    </div>
</div>
