<div class="header-1">
    <div class="page-header" style="background-image: url({{env('MIX_BUCKET_BASE_URL') . '/' . env('MIX_BUCKET_NAME') . '/' . $data['image']['src']}});">
        <div class="filter"></div>
        <div class="content-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        @if(!empty($data['video-link']))
                            <div class="iframe-container">
                                <iframe src="{{$data['video-link']}}" frameborder="0" allowfullscreen
                                        height="250"></iframe>
                            </div>
                        @endif
                    </div>
                    <div class="col-md-6  ml-auto">
                        @if(!empty($data['title']))
                            <h2 class="title">{{$data['title']}}</h2>
                        @endif
                        @if(!empty($data['description']))
                            <h5 class="description">{{$data['description']}}</h5>
                        @endif
                        <br/>
                        @if(!empty($data['label']))
                            <a href="{{$data['action'] ?: '#'}}" target="{{$data['target'] ?: '_self'}}"
                               class="btn btn-danger">
                                {{$data['label']}}
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
