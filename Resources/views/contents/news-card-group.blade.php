<div class="blog-5">
    <div class="container">
        @if(!empty($data['title']))
            <h2 class="title text-center">{!! $data['title'] !!}</h2>
        @endif
        <div class="row">
            @livewire('news-card-group', ['targets' => $targets])
        </div>
    </div>
</div>
