<div class="testimonials-3">
    <div class="container">
        <div class="row">
            <div class="col-md-6 ml-auto mr-auto text-center">
                @if(!empty($data['title']))
                    <h2 class="title">{{$data['title']}}</h2>
                @endif
            </div>
        </div>
        <div class="row">
            @livewire('testimonial-card-group', ['targets' => $targets])
        </div>
    </div>
</div>
