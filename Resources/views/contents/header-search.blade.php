<div class="header-2">
    <div class="page-header"
         style="background-image: url({{!empty($data['image'])
                    ? (env('MIX_BUCKET_BASE_URL') .'/'. env('MIX_BUCKET_NAME') .'/'. $data['image']['src'])
                    : asset('assets_navi/img/image_placeholder.jpg')}});">
        <div class="filter"></div>
        <div class="content-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                        @if(!empty($data['title']))
                            <h1 class="title">{{$data['title']}}</h1>
                        @endif
                        @if(!empty($data['description']))
                            <h5 class="description">{{$data['description']}}</h5>
                        @endif
                        <br/>
                    </div>
                    <div class="col-md-10 ml-auto mr-auto">
                        <div class="card card-raised card-form-horizontal no-transition">
                            <div class="card-body">
                                <form method="post" action="#">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="text" value="" placeholder="City" class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="text" value="" placeholder="Country" class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="text" value="" placeholder="Date" class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <button type="button" class="btn btn-danger btn-block"><i
                                                    class="nc-icon nc-zoom-split"></i> &nbsp; Search
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
