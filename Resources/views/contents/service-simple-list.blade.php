<div class="features-1">
    <div class="container">
        <div class="row">
            <div class="col-md-8 ml-auto mr-auto text-center">
                @if(!empty($data['title']))
                    <h2 class="title">{{$data['title']}}</h2>
                @endif
                @if(!empty($data['description']))
                    <h5 class="description">{{$data['description']}}</h5>
                @endif
            </div>
        </div>
        @livewire('service-simple-list', ['targets' => $targets])
    </div>
</div>
