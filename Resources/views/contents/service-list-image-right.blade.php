<div class="features-3">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="container">
                        @if($data['title'])
                            <h2 class="title">{!! $data['title'] !!}</h2>
                        @endif
                    </div>
                </div>
                <div class="row">
                    @livewire('service-list-image-right', ['targets' => $targets])
                </div>
            </div>
            <div class="col-md-5 ml-auto">
                <div class="iphone-container">
                    <x-image-component :picture="$data['image']"
                                       :src="asset('assets_navi/img/placeholder.jpg')">
                    </x-image-component>
                </div>
            </div>
        </div>
    </div>
</div>
