<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">
<head>
    @if(!empty($optimizeSettings['enabled']))
        @include('sitebuilder::partials.optimize-anti-flicker', ['container_id' => $optimizeSettings['container_id']])
        @include('sitebuilder::partials.ga-tag', ['container_id' => $optimizeSettings['container_id'], 'property_id' => $optimizeSettings['property_id']])
    @endif
    @if(!empty($gtmSettings['enabled']))
        @include('sitebuilder::partials.gtm-head', ['container_id' => $gtmSettings['container_id']])
    @endif
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>{{$metaTitle}}</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <meta name="description" content="{{$metaDescription}}"/>

    @if(!empty($faviconsData))
        @include('sitebuilder::partials.favicon', ['faviconsData' => $faviconsData])
    @endif

    <!-- CSS Files -->
    <link href="{{ mix('assets_navi/css/main.min.css')}}" rel="stylesheet"/>

    @livewireStyles
</head>

<body class="index-page">
@if(!empty($gtmSettings['enabled']))
    @include('sitebuilder::partials.gtm-body', ['container_id' => $gtmSettings['container_id']])
@endif
{{--@include('navi::partials.header')--}}
<!-- Navbar -->
<nav class="navbar navbar-expand-lg fixed-top navbar-default" color-on-scroll="500">
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand" href="https://demos.creative-tim.com/paper-kit-2-pro/index.html" rel="tooltip"
               title="Paper Kit 2 PRO" data-placement="bottom" target="_blank">
                Paper Kit 2 Pro
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
                    aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" data-nav-image="./assets_navi/img/blurred-image-1.jpg"
             data-color="orange">
            <ul class="navbar-nav ml-auto">
                <li class="dropdown nav-item">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false">
                        Link 1 </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-danger">
                        <a href="javascript:" class="dropdown-item">
                            Sub link 1
                        </a>
                        <a href="javascript:" class="dropdown-item">
                            Sub link 2
                        </a>
                        <a href="javascript:" class="dropdown-item">
                            Sub link 3
                        </a>
                    </div>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link" id="navbarDropdownMenuLink1" data-toggle="dropdown">
                        Link 2
                    </a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-round btn-danger" href="https://www.creative-tim.com/product/paper-kit-2-pro"
                       target="_blank">
                        <i class="nc-icon nc-cart-simple"></i> Link 3
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- End Navbar -->
<div class="wrapper" style="height:100vh;">
    <div class="section">
        @yield('content')
    </div>
</div>

{{--@include('navi::partials.footer')--}}
<footer class="footer footer-black">
    <div class="container-fluid">
        <div class="row">
            <nav class="footer-nav">
                <ul>
                    <li><a href="https://www.creative-tim.com" target="_blank">Creative Tim</a></li>
                    <li><a href="https://www.creative-tim.com/blog" target="_blank">Blog</a></li>
                    <li><a href="https://www.creative-tim.com/license" target="_blank">Licenses</a></li>
                </ul>
            </nav>
            <div class="credits ml-auto">
                      <span class="copyright">
                        © 2020, made with <i class="fa fa-heart heart"></i> by Creative Tim
                      </span>
            </div>
        </div>
    </div>
</footer>

<script type="text/javascript" src="{{ mix('assets_navi/js/all-scripts.min.js')}}"></script>

@include('sitebuilder::partials.lazyload')

@stack('scripts')

@livewireScripts

</body>
</html>
