<?php

namespace OctoCmsModule\Navi\Providers;

use Config;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Livewire\Livewire;
use OctoCmsModule\Navi\Console\InstallNaviCommand;

class NaviServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Navi';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'navi';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));

        $this->commands(
            [
            InstallNaviCommand::class,
            ]
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);

        Livewire::component(
            'news-vertical-cards',
            getBindingImplementation($this->moduleName, "Http\Livewire\NewsVerticalCards")
        );
        Livewire::component(
            'service-simple-list',
            getBindingImplementation($this->moduleName, "Http\Livewire\ServiceSimpleList")
        );
        Livewire::component(
            'service-list-image-left',
            getBindingImplementation($this->moduleName, "Http\Livewire\ServiceListImageLeft")
        );
        Livewire::component(
            'service-card-list',
            getBindingImplementation($this->moduleName, "Http\Livewire\ServiceCardList")
        );
        Livewire::component(
            'service-list-image-right',
            getBindingImplementation($this->moduleName, "Http\Livewire\ServiceListImageRight")
        );
        Livewire::component(
            'news-list-big',
            getBindingImplementation($this->moduleName, "Http\Livewire\NewsListBig")
        );
        Livewire::component(
            'news-list-small',
            getBindingImplementation($this->moduleName, "Http\Livewire\NewsListSmall")
        );
        Livewire::component(
            'news-pairs',
            getBindingImplementation($this->moduleName, "Http\Livewire\NewsPairs")
        );
        Livewire::component(
            'news-card-group',
            getBindingImplementation($this->moduleName, "Http\Livewire\NewsCardGroup")
        );
        Livewire::component(
            'testimonial-card-list',
            getBindingImplementation($this->moduleName, "Http\Livewire\TestimonialCardList")
        );
        Livewire::component(
            'testimonial-card-group',
            getBindingImplementation($this->moduleName, "Http\Livewire\TestimonialCardGroup")
        );
        Livewire::component(
            'testimonial-slider',
            getBindingImplementation($this->moduleName, "Http\Livewire\TestimonialSlider")
        );
        Livewire::component(
            'testimonial-slide-number',
            getBindingImplementation($this->moduleName, "Http\Livewire\TestimonialSlideNumber")
        );
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes(
            [
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
            ], 'config'
        );
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'),
            $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes(
            [
            $sourcePath => $viewPath,
            ], ['views', $this->moduleNameLower . '-module-views']
        );

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path($this->moduleName, 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
