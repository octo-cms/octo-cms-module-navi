<?php

namespace OctoCmsModule\Navi\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use OctoCmsModule\Sitebuilder\Entities\BlockEntity;
use OctoCmsModule\Sitebuilder\Entities\BlockHtml;

/**
 * Class InstallNaviCommand
 *
 * @package OctoCmsModule\Navi\Console
 */
class InstallNaviCommand extends Command
{
    protected const MODULE_NAME = 'Navi';

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'install:navi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->info('Running Install Navi Command ...');

        $this->info('Creating BlockHtml ...');

        foreach (NaviHtmlBlocks::BLOCKS as $htmlBlock) {
            $blade = Arr::get($htmlBlock, 'blade', '');
            BlockHtml::updateOrCreate(
                [
                    'blade'  => $blade,
                    'module' => self::MODULE_NAME,
                ],
                [
                    'target'       => BlockHtml::TARGET_PAGE,
                    'settings'     => Arr::get($htmlBlock, 'values', ''),
                    'instructions' => Arr::get($htmlBlock, 'instructions', ''),
                    'layout'       => Arr::get($htmlBlock, 'layout', ''),
                    'src'          => env('MIX_BUCKET_BASE_URL') . "/octo-cms-dist/Navi/$blade.png",
                ]
            );
        }

        $this->info('Creating BlockEntity ...');

        foreach (NaviEntityBlocks::BLOCKS as $entityBlock) {
            $blade = Arr::get($entityBlock, 'blade', '');
            BlockEntity::updateOrCreate(
                [
                    'blade'  => $blade,
                    'module' => self::MODULE_NAME,
                    'entity' => Arr::get($entityBlock, 'entity', ''),
                ],
                [
                    'target'       => BlockEntity::TARGET_PAGE,
                    'settings'     => Arr::get($entityBlock, 'values', []),
                    'instructions' => Arr::get($entityBlock, 'instructions', ''),
                    'layout'       => Arr::get($entityBlock, 'layout', ''),
                    'src'          => env('MIX_BUCKET_BASE_URL') . "/octo-cms-dist/Navi/$blade.png",
                ]
            );
        }
    }
}
