<?php

namespace OctoCmsModule\Navi\Console;

/**
 * Class NaviHtmlBlocks
 *
 * @category Octo
 * @package  OctoCmsModule\Navi\Console
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class NaviHtmlBlocks
{
    public const TYPE_STRING = 'string';
    public const TYPE_IMAGE = 'image';

    public const INSTRUCTIONS_ACTION = 'URL della pagina a cui va il collegamento del pulsante';
    public const INSTRUCTIONS_TARGET = 'Modalità in cui viene aperto il collegamento';

    public const BLOCKS = [
        [
            'blade'        => 'header-video',
            'instructions' => 'Un video verrà mostrato su questo blocco compilando la variabile "video-link"',
            'values'       => [
                ['type' => self::TYPE_STRING, 'name' => 'video-link', 'instructions' => 'scrivere link
                embedded per mostrare il video'],
                ['type' => self::TYPE_STRING, 'name' => 'title', 'instructions' => ''],
                ['type' => self::TYPE_STRING, 'name' => 'description', 'instructions' => ''],
                ['type' => self::TYPE_STRING, 'name' => 'action', 'instructions' => self::INSTRUCTIONS_ACTION],
                ['type' => self::TYPE_STRING, 'name' => 'target', 'instructions' => self::INSTRUCTIONS_TARGET],
                ['type' => self::TYPE_STRING, 'name' => 'label', 'instructions' => 'Testo del pulsante'],
                ['type' => self::TYPE_IMAGE, 'name' => 'image', 'instructions' => 'dimensioni consigliate: 1200x900
                (rapporto 4:3)'],
            ],
            'layout'       => [],
        ],
        [
            'blade'        => 'header-search',
            'instructions' => 'Funzionalità del form ancora da decidere, si possono comunque compilare
            tutte le variabili',
            'values'       => [
                ['type' => self::TYPE_STRING, 'name' => 'title', 'instructions' => ''],
                ['type' => self::TYPE_STRING, 'name' => 'description', 'instructions' => ''],
                ['type' => self::TYPE_IMAGE, 'name' => 'image', 'instructions' => 'dimensioni consigliate: 1200x800
                (rapporto 3:2)'],
            ],
            'layout'       => [],
        ],
    ];
}
