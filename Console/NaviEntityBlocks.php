<?php

namespace OctoCmsModule\Navi\Console;

/**
 * Class NaviEntityBlocks
 *
 * @category Octo
 * @package  OctoCmsModule\Navi\Console
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class NaviEntityBlocks
{
    public const TYPE_STRING = 'string';
    public const TYPE_IMAGE = 'image';

    public const BLOCKS = [
        [
            'blade'        => 'news-vertical-cards',
            'instructions' => 'Accetta al massimo tre (3) news.',
            'entity'       => 'News',
            'layout'       => [],
            'values'       => [
                ['type' => self::TYPE_STRING, 'name' => 'title', 'instructions' => ''],
            ],
        ],
        [
            'blade'        => 'service-simple-list',
            'instructions' => 'Accetta al massimo (4) servizi.',
            'entity'       => 'Service',
            'layout'       => [],
            'values'       => [
                ['type' => self::TYPE_STRING, 'name' => 'title', 'instructions' => ''],
                ['type' => self::TYPE_STRING, 'name' => 'description', 'instructions' => ''],
            ],
        ],
        [
            'blade'        => 'service-list-image-left',
            'instructions' => 'Accetta al massimo tre (3) news',
            'entity'       => 'Service',
            'layout'       => [],
            'values'       => [
                ['type' => self::TYPE_STRING, 'name' => 'title', 'instructions' => ''],
                ['type' => self::TYPE_STRING, 'name' => 'description', 'instructions' => ''],
                ['type' => self::TYPE_IMAGE, 'name' => 'image', 'instructions' => 'dimensioni consigliate:
                500x600 (rapporto 5:6)'],
            ],
        ],
        [
            'blade'        => 'service-card-list',
            'instructions' => 'Accetta al massimo tre (3) servizi',
            'entity'       => 'Service',
            'layout'       => [],
            'values'       => [
                ['type' => self::TYPE_STRING, 'name' => 'title', 'instructions' => ''],
                ['type' => self::TYPE_STRING, 'name' => 'description', 'instructions' => ''],
            ],
        ],
        [
            'blade'        => 'service-list-image-right',
            'instructions' => 'Accetta al massimo tre (3) news',
            'entity'       => 'Service',
            'layout'       => [],
            'values'       => [
                ['type' => self::TYPE_STRING, 'name' => 'title', 'instructions' => ''],
                ['type' => self::TYPE_IMAGE, 'name' => 'image', 'instructions' => 'dimensioni consigliate:
                200x600 (rapporto 1:3)'],
            ],
        ],
        [
            'blade'        => 'news-list-big',
            'instructions' => 'Lista delle news con immagini grandi.',
            'entity'       => 'News',
            'layout'       => [],
            'values'       => [
                ['type' => self::TYPE_STRING, 'name' => 'title', 'instructions' => ''],
            ],
        ],
        [
            'blade'        => 'news-list-small',
            'instructions' => 'Lista delle news con immagini piccole.',
            'entity'       => 'News',
            'layout'       => [],
            'values'       => [
                ['type' => self::TYPE_STRING, 'name' => 'title', 'instructions' => ''],
            ],
        ],
        [
            'blade'        => 'news-pairs',
            'instructions' => 'Lista delle news che mostra due news alla volta per ogni riga.',
            'entity'       => 'News',
            'layout'       => [],
            'values'       => [
                ['type' => self::TYPE_STRING, 'name' => 'title', 'instructions' => ''],
            ],
        ],
        [
            'blade'        => 'news-card-group',
            'instructions' => 'Fortemente consigliato usare l\'opzione newest o aggiungere cinque (5)
            news custom',
            'entity'       => 'News',
            'layout'       => [],
            'values'       => [
                ['type' => self::TYPE_STRING, 'name' => 'title', 'instructions' => ''],
            ],
        ],
        [
            'blade'        => 'testimonial-card-list',
            'instructions' => 'Accetta al massimo tre (3) testimonials',
            'entity'       => 'Testimonial',
            'layout'       => [],
            'values'       => [
                ['type' => self::TYPE_STRING, 'name' => 'title', 'instructions' => ''],
                ['type' => self::TYPE_STRING, 'name' => 'description', 'instructions' => ''],
                ['type' => self::TYPE_IMAGE, 'name' => 'image', 'instructions' => 'dimensioni consigliate:
                1200x800 (rapporto 3:2)'],
            ],
        ],
        [
            'blade'        => 'testimonial-card-group',
            'instructions' => 'Fortemente consigliato usare l\'opzione newest o aggiungere cinque (5)
            testimonials custom',
            'entity'       => 'Testimonial',
            'layout'       => [],
            'values'       => [
                ['type' => self::TYPE_STRING, 'name' => 'title', 'instructions' => ''],
            ],
        ],
        [
            'blade'        => 'testimonial-slider',
            'instructions' => 'Acetta al massimo cinque (5) testimonials',
            'entity'       => 'Testimonial',
            'layout'       => [],
            'values'       => [],
        ],
    ];
}
