<?php

namespace OctoCmsModule\Navi\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;
use OctoCmsModule\Sitebuilder\Utils\LivewireUtils;
use OctoCmsModule\Testimonials\Entities\Testimonial;

/**
 * Class TestimonialSlideNumber
 *
 * @package OctoCmsModule\Navi\Http\Livewire
 */
class TestimonialSlideNumber extends Component
{
    public const LIMIT = 5;

    public $targets = [];

    private $livewireUtils;

    /**
     * @param LivewireUtils $livewireUtils
     * @param array $targets
     */
    public function mount(LivewireUtils $livewireUtils, $targets = [])
    {
        $this->livewireUtils = $livewireUtils;
        $this->targets = $targets;
    }

    /**
     * @return int
     */
    public function getTestimonialCountProperty()
    {
        /** @var Testimonial[] $entities */
        $entities = $this->livewireUtils->getEntities(
            Testimonial::query(),
            $this->targets
        );

        if (empty($entities)) {
            return 0;
        }

        return count($entities) < self::LIMIT ? count($entities) : self::LIMIT;
    }


    /**
     * @return Application|Factory|View
     */
    public function render()
    {
        return view()->first([
            'livewire.testimonial-slide-number',
            'navi::livewire.testimonial-slide-number',
        ]);
    }
}
