<?php

namespace OctoCmsModule\Navi\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Collection;
use Illuminate\View\View;
use Livewire\Component;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Core\Traits\ImageSrcTrait;
use OctoCmsModule\Core\Utils\LanguageUtils;
use OctoCmsModule\Sitebuilder\Traits\PageUrlTrait;
use OctoCmsModule\Sitebuilder\Utils\LivewireUtils;

/**
 * Class NewsCardGroup
 *
 * @package OctoCmsModule\Navi\Http\Livewire
 */
class NewsCardGroup extends Component
{
    use ImageSrcTrait, PageUrlTrait;

    public const LIMIT = 5;

    public const COL_SIZES = ['7', '5', '3', '6', '3'];

    public $targets = [];

    private $livewireUtils;

    /**
     * @param LivewireUtils $livewireUtils
     * @param array         $targets
     */
    public function mount(LivewireUtils $livewireUtils, $targets = [])
    {
        $this->livewireUtils = $livewireUtils;
        $this->targets = $targets;
    }

    /**
     * @return array|null
     */
    public function getNewsProperty()
    {
        /** @var News[] $news */
        $entities = $this->livewireUtils->getEntities(
            News::with('newsLangs')
                ->with(['page', 'page.pageLangs'])
                ->with('pictures')
                ->with('categories'),
            $this->targets
        );

        /** @var array $newsToPublish */
        $newsToPublish = [];

        $count = 0;

        foreach ($entities as $entity) {
            if ($count >= self::LIMIT) {
                break;
            }

            /** @var Collection $categoryLangs */
            $categoryLangs = optional($entity->categories->first())->categoryLangs ?: collect([]);

            $newsToPublish[] = [
                'id'                => $entity->id,
                'author'            => $entity->author,
                'page'              => $entity->page,
                'picture'           => optional($entity->getPictures('main')->first())->toArray(),
                'title'             => LanguageUtils::getLangValue($entity->newsLangs, 'title'),
                'short_description' => LanguageUtils::getLangValue($entity->newsLangs, 'short_description'),
                'category'          => LanguageUtils::getLangValue($categoryLangs, 'name'),
                'colSize'           => self::COL_SIZES[$count],
            ];

            $count++;
        }

        return $newsToPublish;
    }

    /**
     * @return Application|Factory|View
     */
    public function render()
    {
        return view()->first([
            'livewire.news-card-group',
            'navi::livewire.news-card-group',
        ]);
    }
}
