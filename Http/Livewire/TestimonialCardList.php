<?php

namespace OctoCmsModule\Navi\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;
use OctoCmsModule\Core\Entities\Picture;
use OctoCmsModule\Core\Traits\ImageSrcTrait;
use OctoCmsModule\Core\Utils\LanguageUtils;
use OctoCmsModule\Sitebuilder\Utils\LivewireUtils;
use OctoCmsModule\Testimonials\Entities\Testimonial;

/**
 * Class TestimonialCardList
 *
 * @package OctoCmsModule\Navi\Http\Livewire
 */
class TestimonialCardList extends Component
{
    use ImageSrcTrait;

    public const LIMIT = 3;

    public $targets = [];

    private $livewireUtils;

    /**
     * @param LivewireUtils $livewireUtils
     * @param array $targets
     */
    public function mount(LivewireUtils $livewireUtils, $targets = [])
    {
        $this->livewireUtils = $livewireUtils;
        $this->targets = $targets;
    }

    /**
     * @return array|null
     */
    public function getTestimonialsProperty()
    {
        /** @var Testimonial[] $entities */
        $entities = $this->livewireUtils->getEntities(
            Testimonial::with('testimonialLangs')
                ->with('pictures'),
            $this->targets
        );

        if (empty($entities)) {
            return [];
        }

        /** @var array $testimonialsToPublish */
        $testimonialsToPublish = [];

        /** @var int $count */
        $count = 0;

        foreach ($entities as $entity) {
            /** @var Picture $picture */
            $picture = $entity->getPictures('main')->first();

            if ($count >= self::LIMIT) {
                break;
            }

            $testimonialsToPublish[] = [
                'id'      => $entity->id,
                'author'  => $entity->author,
                'picture' => !empty($picture) ? $picture->load('pictureLangs')->toArray() : null,
                'text'    => LanguageUtils::getLangValue($entity->testimonialLangs, 'text'),
                'job'     => LanguageUtils::getLangValue($entity->testimonialLangs, 'job'),
            ];

            $count++;
        }

        return $testimonialsToPublish;
    }


    /**
     * @return Application|Factory|View
     */
    public function render()
    {
        return view()->first([
            'livewire.testimonial-card-list',
            'navi::livewire.testimonial-card-list',
        ]);
    }
}
