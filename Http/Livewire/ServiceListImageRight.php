<?php

namespace OctoCmsModule\Navi\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;
use OctoCmsModule\Core\Entities\Picture;
use OctoCmsModule\Core\Utils\LanguageUtils;
use OctoCmsModule\Services\Entities\Service;
use OctoCmsModule\Sitebuilder\Traits\PageUrlTrait;
use OctoCmsModule\Sitebuilder\Utils\LivewireUtils;

/**
 * Class ServiceListImageRight
 *
 * @package OctoCmsModule\Navi\Http\Livewire
 */
class ServiceListImageRight extends Component
{
    use PageUrlTrait;

    public const LIMIT = 4;

    public $targets = [];

    private $livewireUtils;

    /**
     * @param LivewireUtils $livewireUtils
     * @param array $targets
     */
    public function mount(LivewireUtils $livewireUtils, $targets = [])
    {
        $this->livewireUtils = $livewireUtils;
        $this->targets = $targets;
    }

    /**
     * @return array|null
     */
    public function getServicesProperty()
    {
        /** @var Service[] $entities */
        $entities = $this->livewireUtils->getEntities(
            Service::with('serviceLangs')
                ->with(['page', 'page.pageLangs'])
                ->with('pictures'),
            $this->targets
        );

        if (empty($entities)) {
            return [];
        }

        /** @var array $servicesToPublish */
        $servicesToPublish = [];

        /** @var int $count */
        $count = 0;

        foreach ($entities as $entity) {
            /** @var Picture $customIcon */
            $customIcon = $entity->getPictures('icon')->first();

            if ($count >= self::LIMIT) {
                break;
            }

            $servicesToPublish[] = [
                'id'          => $entity->id,
                'icon'        => $entity->icon,
                'customIcon'  => !empty($customIcon) ? $customIcon->load('pictureLangs')->toArray() : null,
                'page'        => !empty($entity->page) ? $this->getPageUrl($entity->page) : '#',
                'name'        => LanguageUtils::getLangValue($entity->serviceLangs, 'name'),
                'description' => LanguageUtils::getLangValue($entity->serviceLangs, 'description'),
            ];

            $count++;
        }

        return $servicesToPublish;
    }


    /**
     * @return Application|Factory|View
     */
    public function render()
    {
        return view()->first([
            'livewire.service-list-image-right',
            'navi::livewire.service-list-image-right',
        ]);
    }
}
