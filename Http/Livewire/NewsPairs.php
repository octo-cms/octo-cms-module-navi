<?php

namespace OctoCmsModule\Navi\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Collection;
use Illuminate\View\View;
use Livewire\Component;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Core\Utils\LanguageUtils;
use OctoCmsModule\Sitebuilder\Traits\PageUrlTrait;
use OctoCmsModule\Sitebuilder\Utils\LivewireUtils;

/**
 * Class NewsPairs
 *
 * @package OctoCmsModule\Navi\Http\Livewire
 */
class NewsPairs extends Component
{
    use PageUrlTrait;

    public $targets = [];

    private $livewireUtils;

    /**
     * @param LivewireUtils $livewireUtils
     * @param array $targets
     */
    public function mount(LivewireUtils $livewireUtils, $targets = [])
    {
        $this->livewireUtils = $livewireUtils;
        $this->targets = $targets;
    }

    /**
     * @return array
     */
    public function getNewsProperty()
    {
        /** @var News[] $news */
        $entities = $this->livewireUtils->getEntities(
            News::with('newsLangs')
                ->with(['page', 'page.pageLangs'])
                ->with('pictures')
                ->with('categories'),
            $this->targets
        );

        if (empty($entities)) {
            return [];
        }

        $newsToPublish = [];

        foreach ($entities as $entity) {
            $picture = $entity->getPictures()->first();

            /** @var Collection $categoryLangs */
            $categoryLangs = optional($entity->categories->first())->categoryLangs ?: collect([]);

            $newsToPublish[] = [
                'id'                => $entity->id,
                'picture'           => !empty($picture) ? $picture->load('pictureLangs')->toArray() : null,
                'page'              => !empty($entity->page) ? $this->getPageUrl($entity->page) : '#',
                'title'             => LanguageUtils::getLangValue($entity->newsLangs, 'title'),
                'short_description' => LanguageUtils::getLangValue($entity->newsLangs, 'short_description'),
                'category'          => LanguageUtils::getLangValue($categoryLangs, 'name'),
            ];
        }

        return $newsToPublish;
    }

    /**
     * @return Application|Factory|View
     */
    public function render()
    {
        return view()->first([
            'livewire.news-pairs',
            'navi::livewire.news-pairs',
        ]);
    }
}
