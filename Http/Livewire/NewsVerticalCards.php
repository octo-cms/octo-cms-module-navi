<?php

namespace OctoCmsModule\Navi\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Collection;
use Illuminate\View\View;
use Livewire\Component;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Core\Utils\LanguageUtils;
use OctoCmsModule\Sitebuilder\Traits\PageUrlTrait;
use OctoCmsModule\Sitebuilder\Utils\LivewireUtils;

/**
 * Class NewsVerticalCards
 *
 * @package OctoCmsModule\Navi\Http\Livewire
 */
class NewsVerticalCards extends Component
{
    use PageUrlTrait;

    public const LIMIT = 3;

    public $targets = [];

    private $livewireUtils;

    /**
     * @param LivewireUtils $livewireUtils
     * @param array $targets
     */
    public function mount(LivewireUtils $livewireUtils, $targets = [])
    {
        $this->livewireUtils = $livewireUtils;
        $this->targets = $targets;
    }

    /**
     * @return array|null
     */
    public function getNewsProperty()
    {
        /** @var News[] $news */
        $entities = $this->livewireUtils->getEntities(
            News::with('newsLangs')
                ->with(['page', 'page.pageLangs'])
                ->with('pictures')
                ->with('categories'),
            $this->targets
        );

        if (empty($entities)) {
            return [];
        }

        $newsToPublish = [];

        $count = 0;

        foreach ($entities as $entity) {
            $picture = $entity->getPictures()->first();

            /** @var Collection $categoryLangs */
            $categoryLangs = optional($entity->categories->first())->categoryLangs ?: collect([]);

            if ($count >= self::LIMIT) {
                break;
            }

            $newsToPublish[] = [
                'id'                => $entity->id,
                'author'            => $entity->author,
                'date'              => $entity->date->format('d/m/Y'),
                'picture'           => !empty($picture) ? $picture->load('pictureLangs')->toArray() : null,
                'page'              => !empty($entity->page) ? $this->getPageUrl($entity->page) : '#',
                'title'             => LanguageUtils::getLangValue($entity->newsLangs, 'title'),
                'short_description' => LanguageUtils::getLangValue($entity->newsLangs, 'short_description'),
                'category'          => LanguageUtils::getLangValue($categoryLangs, 'name'),
            ];

            $count++;
        }

        return $newsToPublish;
    }


    /**
     * @return Application|Factory|View
     */
    public function render()
    {
        return view()->first([
            'livewire.news-vertical-cards',
            'navi::livewire.news-vertical-cards',
        ]);
    }
}
