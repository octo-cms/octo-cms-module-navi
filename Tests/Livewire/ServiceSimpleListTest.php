<?php

namespace OctoCmsModule\Navi\Tests\Livewire;

use Livewire\Livewire;
use OctoCmsModule\Services\Entities\Service;
use OctoCmsModule\Services\Entities\ServiceLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class ServiceSimpleListTest
 *
 * @package OctoCmsModule\Navi\Tests\Livewire
 */
class ServiceSimpleListTest extends TestCase
{


    public function test_getServiceArray()
    {
        Service::factory()
            ->count(5)
            ->has(
                ServiceLang::factory()
                    ->state(function (array $attributes, Service $service) {
                        return [
                            'lang'        => 'it',
                            'name'        => 'nome-' . $service->id,
                            'description' => 'descrizione-' . $service->id,
                        ];
                    })
            )->has(
                ServiceLang::factory()
                    ->state(function (array $attributes, Service $service) {
                        return [
                            'lang'        => 'en',
                            'name'        => 'name-' . $service->id,
                            'description' => 'description-' . $service->id,
                        ];
                    })
            )
            ->create();


        $targets = [
            'type'   => 'custom',
            'values' => [
                ['id' => 1],
                ['id' => 2],
                ['id' => 3],
                ['id' => 4],
                ['id' => 5],
            ],
        ];

        Livewire::test('service-simple-list', ['targets' => $targets])
            ->assertSet('services', [
                [
                    'id'          => 1,
                    'name'        => 'nome-1',
                    'description' => 'descrizione-1',
                    'icon'        => null,
                    'customIcon'  => null,
                    'page'        => '#',
                ],
                [
                    'id'          => 2,
                    'name'        => 'nome-2',
                    'description' => 'descrizione-2',
                    'icon'        => null,
                    'customIcon'  => null,
                    'page'        => '#',
                ],
                [
                    'id'          => 3,
                    'name'        => 'nome-3',
                    'description' => 'descrizione-3',
                    'icon'        => null,
                    'customIcon'  => null,
                    'page'        => '#',
                ],
                [
                    'id'          => 4,
                    'name'        => 'nome-4',
                    'description' => 'descrizione-4',
                    'icon'        => null,
                    'customIcon'  => null,
                    'page'        => '#',
                ],
            ]);
    }
}
