<?php

namespace OctoCmsModule\Navi\Tests\Livewire;

use Carbon\Carbon;
use Livewire\Livewire;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Entities\NewsLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class NewsVerticalCardsTest
 *
 * @package OctoCmsModule\Navi\Tests\Livewire
 */
class NewsVerticalCardsTest extends TestCase
{


    public function test_getNewsArray()
    {
        News::factory()
            ->state(['author' => 'author'])
            ->count(5)
            ->has(
                NewsLang::factory()
                    ->state(function (array $attributes, News $news) {
                        return [
                            'lang'              => 'it',
                            'title'             => 'titolo-' . $news->id,
                            'short_description' => 'descrizione-breve-' . $news->id,
                        ];
                    })
            )->has(
                NewsLang::factory()
                    ->state(function (array $attributes, News $news) {
                        return [
                            'lang'              => 'en',
                            'title'             => 'title-' . $news->id,
                            'short_description' => 'short-description-' . $news->id,
                        ];
                    })
            )
            ->create(['date' => Carbon::now()]);


        $targets = [
            'type'   => 'custom',
            'values' => [
                0 => ['id' => 1],
                1 => ['id' => 2],
                2 => ['id' => 3],
                3 => ['id' => 4],
            ],
        ];

        Livewire::test('news-vertical-cards', ['targets' => $targets])
            ->assertSet('news', [
                [
                    'id'                => 1,
                    'author'            => 'author',
                    'title'             => 'titolo-1',
                    'date'              => Carbon::now()->format('d/m/Y'),
                    'short_description' => 'descrizione-breve-1',
                    'picture'           => null,
                    'category'          => '',
                    'page'              => '#',
                ],
                [
                    'id'                => 2,
                    'author'            => 'author',
                    'title'             => 'titolo-2',
                    'date'              => Carbon::now()->format('d/m/Y'),
                    'short_description' => 'descrizione-breve-2',
                    'picture'           => null,
                    'category'          => '',
                    'page'              => '#',
                ],
                [
                    'id'                => 3,
                    'author'            => 'author',
                    'title'             => 'titolo-3',
                    'date'              => Carbon::now()->format('d/m/Y'),
                    'short_description' => 'descrizione-breve-3',
                    'picture'           => null,
                    'category'          => '',
                    'page'              => '#',
                ],
            ]);
    }
}
