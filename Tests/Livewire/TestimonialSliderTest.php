<?php

namespace OctoCmsModule\Navi\Tests\Livewire;

use Livewire\Livewire;
use OctoCmsModule\Testimonials\Entities\Testimonial;
use OctoCmsModule\Testimonials\Entities\TestimonialLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class TestimonialSliderTest
 *
 * @package OctoCmsModule\Navi\Tests\Livewire
 */
class TestimonialSliderTest extends TestCase
{


    public function test_getTestimonialArray()
    {
        Testimonial::factory()
            ->count(4)
            ->has(
                TestimonialLang::factory()
                    ->state(function (array $attributes, Testimonial $service) {
                        return [
                            'lang' => 'it',
                            'job'  => 'lavoro-' . $service->id,
                            'text' => 'testo-' . $service->id,
                        ];
                    })
            )->has(
                TestimonialLang::factory()
                    ->state(function (array $attributes, Testimonial $service) {
                        return [
                            'lang' => 'en',
                            'job'  => 'job-' . $service->id,
                            'text' => 'text-' . $service->id,
                        ];
                    })
            )
            ->create(['author' => 'author']);


        $targets = [
            'type'   => 'custom',
            'values' => [
                ['id' => 1],
                ['id' => 2],
                ['id' => 3],
            ],
        ];

        Livewire::test('testimonial-slider', ['targets' => $targets])
            ->assertSet('testimonials', [
                [
                    'id'      => 1,
                    'author'  => 'author',
                    'job'     => 'lavoro-1',
                    'text'    => 'testo-1',
                    'picture' => null,
                ],
                [
                    'id'      => 2,
                    'author'  => 'author',
                    'job'     => 'lavoro-2',
                    'text'    => 'testo-2',
                    'picture' => null,
                ],
                [
                    'id'      => 3,
                    'author'  => 'author',
                    'job'     => 'lavoro-3',
                    'text'    => 'testo-3',
                    'picture' => null,
                ],
            ]);
    }
}
