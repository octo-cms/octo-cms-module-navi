<?php

namespace OctoCmsModule\Navi\Tests\Livewire;

use Carbon\Carbon;
use Livewire\Livewire;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Entities\NewsLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class NewsListSmallTest
 *
 * @package OctoCmsModule\Navi\Tests\Livewire
 */
class NewsListSmallTest extends TestCase
{


    public function test_getNewsArray()
    {
        News::factory()
            ->count(2)
            ->has(
                NewsLang::factory()
                    ->state(function (array $attributes, News $news) {
                        return [
                            'lang'              => 'it',
                            'title'             => 'titolo-' . $news->id,
                            'short_description' => 'descrizione-breve-' . $news->id,
                        ];
                    })
            )->has(
                NewsLang::factory()
                    ->state(function (array $attributes, News $news) {
                        return [
                            'lang'              => 'en',
                            'title'             => 'title-' . $news->id,
                            'short_description' => 'short-description-' . $news->id,
                        ];
                    })
            )
            ->create([
                'author' => 'author',
                'date'   => Carbon::now(),
            ]);


        $targets = [
            'type'   => 'custom',
            'values' => [
                0 => ['id' => 1],
                1 => ['id' => 2],
            ],
        ];

        Livewire::test('news-list-small', ['targets' => $targets])
            ->assertSet('news', [
                [
                    'id'                => 1,
                    'author'            => 'author',
                    'title'             => 'titolo-1',
                    'date'              => Carbon::now()->format('d/m/Y'),
                    'short_description' => 'descrizione-breve-1',
                    'picture'           => null,
                    'category'          => '',
                    'page'              => '#',
                ],
                [
                    'id'                => 2,
                    'author'            => 'author',
                    'title'             => 'titolo-2',
                    'date'              => Carbon::now()->format('d/m/Y'),
                    'short_description' => 'descrizione-breve-2',
                    'picture'           => null,
                    'category'          => '',
                    'page'              => '#',
                ],
            ]);
    }
}
