<?php

namespace OctoCmsModule\Navi\Tests\Livewire;

use Livewire\Livewire;
use OctoCmsModule\Navi\Http\Livewire\TestimonialCardGroup;
use OctoCmsModule\Testimonials\Entities\Testimonial;
use OctoCmsModule\Testimonials\Entities\TestimonialLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class TestimonialCardGroupTest
 *
 * @package OctoCmsModule\Navi\Tests\Livewire
 */
class TestimonialCardGroupTest extends TestCase
{


    public function test_getTestimonialArray()
    {
        Testimonial::factory()
            ->count(3)
            ->has(
                TestimonialLang::factory()
                    ->state(function (array $attributes, Testimonial $service) {
                        return [
                            'lang' => 'it',
                            'text' => 'testo-' . $service->id,
                        ];
                    })
            )->has(
                TestimonialLang::factory()
                    ->state(function (array $attributes, Testimonial $service) {
                        return [
                            'lang' => 'en',
                            'text' => 'text-' . $service->id,
                        ];
                    })
            )
            ->create(['author' => 'author']);


        $targets = [
            'type'   => 'custom',
            'values' => [
                ['id' => 1],
                ['id' => 2],
                ['id' => 3],
            ],
        ];

        Livewire::test('testimonial-card-group', ['targets' => $targets])
            ->assertSet('testimonials', [
                [
                    'id'       => 1,
                    'author'   => 'author',
                    'text'     => 'testo-1',
                    'color'    => TestimonialCardGroup::COLORS[0],
                    'colClass' => TestimonialCardGroup::CLASSES[0],
                    'picture'  => null,
                ],
                [
                    'id'       => 2,
                    'author'   => 'author',
                    'text'     => 'testo-2',
                    'color'    => TestimonialCardGroup::COLORS[1],
                    'colClass' => TestimonialCardGroup::CLASSES[1],
                    'picture'  => null,
                ],
                [
                    'id'       => 3,
                    'author'   => 'author',
                    'text'     => 'testo-3',
                    'color'    => TestimonialCardGroup::COLORS[2],
                    'colClass' => TestimonialCardGroup::CLASSES[2],
                    'picture'  => null,
                ],
            ]);
    }
}
