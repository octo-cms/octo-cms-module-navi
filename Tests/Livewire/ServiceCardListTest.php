<?php

namespace OctoCmsModule\Navi\Tests\Livewire;

use Livewire\Livewire;
use OctoCmsModule\Services\Entities\Service;
use OctoCmsModule\Services\Entities\ServiceLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class ServiceCardListTest
 *
 * @package OctoCmsModule\Navi\Tests\Livewire
 */
class ServiceCardListTest extends TestCase
{


    public function test_getServiceArray()
    {
        Service::factory()
            ->count(4)
            ->has(
                ServiceLang::factory()
                    ->state(function (array $attributes, Service $service) {
                        return [
                            'lang'        => 'it',
                            'name'        => 'nome-' . $service->id,
                            'description' => 'descrizione-' . $service->id,
                        ];
                    })
            )->has(
                ServiceLang::factory()
                    ->state(function (array $attributes, Service $service) {
                        return [
                            'lang'        => 'en',
                            'name'        => 'name-' . $service->id,
                            'description' => 'description-' . $service->id,
                        ];
                    })
            )
            ->create();


        $targets = [
            'type'   => 'custom',
            'values' => [
                ['id' => 1],
                ['id' => 2],
                ['id' => 3],
                ['id' => 4],
            ],
        ];

        Livewire::test('service-card-list', ['targets' => $targets])
            ->assertSet('services', [
                [
                    'id'          => 1,
                    'name'        => 'nome-1',
                    'description' => 'descrizione-1',
                    'icon'        => null,
                    'customIcon'  => null,
                    'picture'     => null,
                    'page'        => '#',
                ],
                [
                    'id'          => 2,
                    'name'        => 'nome-2',
                    'description' => 'descrizione-2',
                    'icon'        => null,
                    'customIcon'  => null,
                    'picture'     => null,
                    'page'        => '#',
                ],
                [
                    'id'          => 3,
                    'name'        => 'nome-3',
                    'description' => 'descrizione-3',
                    'icon'        => null,
                    'customIcon'  => null,
                    'picture'     => null,
                    'page'        => '#',
                ],
            ]);
    }
}
