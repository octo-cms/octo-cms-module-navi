<?php

namespace OctoCmsModule\Navi\Tests\Livewire;

use Livewire\Livewire;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Entities\NewsLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class NewsPairsTest
 *
 * @package OctoCmsModule\Navi\Tests\Livewire
 */
class NewsPairsTest extends TestCase
{


    public function test_getNewsArray()
    {
        News::factory()
            ->count(3)
            ->has(
                NewsLang::factory()
                    ->state(function (array $attributes, News $news) {
                        return [
                            'lang'              => 'it',
                            'title'             => 'titolo-' . $news->id,
                            'short_description' => 'descrizione-breve-' . $news->id,
                        ];
                    })
            )->has(
                NewsLang::factory()
                    ->state(function (array $attributes, News $news) {
                        return [
                            'lang'              => 'en',
                            'title'             => 'title-' . $news->id,
                            'short_description' => 'short-description-' . $news->id,
                        ];
                    })
            )
            ->create();


        $targets = [
            'type'   => 'custom',
            'values' => [
                ['id' => 1],
                ['id' => 2],
                ['id' => 3],
            ],
        ];

        Livewire::test('news-pairs', ['targets' => $targets])
            ->assertSet('news', [
                [
                    'id'                => 1,
                    'title'             => 'titolo-1',
                    'short_description' => 'descrizione-breve-1',
                    'picture'           => null,
                    'category'          => '',
                    'page'              => '#',
                ],
                [
                    'id'                => 2,
                    'title'             => 'titolo-2',
                    'short_description' => 'descrizione-breve-2',
                    'picture'           => null,
                    'category'          => '',
                    'page'              => '#',
                ],
                [
                    'id'                => 3,
                    'title'             => 'titolo-3',
                    'short_description' => 'descrizione-breve-3',
                    'picture'           => null,
                    'category'          => '',
                    'page'              => '#',
                ],
            ]);
    }
}
