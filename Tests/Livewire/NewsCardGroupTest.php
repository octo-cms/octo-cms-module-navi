<?php

namespace OctoCmsModule\Navi\Tests\Livewire;

use Livewire\Livewire;
use OctoCmsModule\Blog\Entities\News;
use OctoCmsModule\Blog\Entities\NewsLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class NewsCardGroupTest
 *
 * @package OctoCmsModule\Navi\Tests\Livewire
 */
class NewsCardGroupTest extends TestCase
{



    public function test_getNewsArray()
    {
        News::factory()
            ->count(2)
            ->has(
                NewsLang::factory()
                    ->state(function (array $attributes, News $news) {
                        return [
                            'lang'              => 'it',
                            'title'             => 'titolo-' . $news->id,
                            'short_description' => 'descrizione-breve-' . $news->id,
                        ];
                    })
            )->has(
                NewsLang::factory()
                    ->state(function (array $attributes, News $news) {
                        return [
                            'lang'              => 'en',
                            'title'             => 'title-' . $news->id,
                            'short_description' => 'short-description-' . $news->id,
                        ];
                    })
            )
            ->create(['author' => 'author']);

        Livewire::test('news-card-group', ['targets' => [
            'type'   => 'custom',
            'values' => [
                ['id' => 1],
                ['id' => 2],
            ],
        ]])
            ->assertSet('news', [
                [
                    'id'                => 1,
                    'author'            => 'author',
                    'title'             => 'titolo-1',
                    'short_description' => 'descrizione-breve-1',
                    'colSize'           => '7',
                    'category'          => '',
                    'picture'           => null,
                    'page'              => null,
                ],
                [
                    'id'                => 2,
                    'author'            => 'author',
                    'title'             => 'titolo-2',
                    'short_description' => 'descrizione-breve-2',
                    'colSize'           => '5',
                    'category'          => '',
                    'picture'           => null,
                    'page'              => null,
                ],
            ]);
    }
}
