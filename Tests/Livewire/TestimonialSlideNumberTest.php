<?php

namespace OctoCmsModule\Navi\Tests\Livewire;

use Livewire\Livewire;
use OctoCmsModule\Navi\Http\Livewire\TestimonialSlideNumber;
use OctoCmsModule\Testimonials\Entities\Testimonial;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class TestimonialSlideNumberTest
 *
 * @package OctoCmsModule\Navi\Tests\Livewire
 */
class TestimonialSlideNumberTest extends TestCase
{


    public function test_getTestimonialCount()
    {
        Testimonial::factory()->count(2)->create();

        $targets = [
            'type'   => 'custom',
            'values' => [
                ['id' => 1],
                ['id' => 2],
            ],
        ];

        Livewire::test('testimonial-slide-number', ['targets' => $targets])
            ->assertSet('testimonialCount', 2);
    }


    public function test_getTestimonialCountOverLimit()
    {
        Testimonial::factory()->count(6)->create();

        $targets = [
            'type'   => 'custom',
            'values' => [
                ['id' => 1],
                ['id' => 2],
                ['id' => 3],
                ['id' => 4],
                ['id' => 5],
                ['id' => 6],
            ],
        ];

        Livewire::test('testimonial-slide-number', ['targets' => $targets])
            ->assertSet('testimonialCount', TestimonialSlideNumber::LIMIT);
    }
}
