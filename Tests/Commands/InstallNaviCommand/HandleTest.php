<?php

namespace OctoCmsModule\Navi\Tests\Commands\InstallNaviCommand;

use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class HandleTest
 *
 * @package OctoCmsModule\Navi\Tests\Commands\InstallNaviCommand
 */
class HandleTest extends TestCase
{


    public function test_installNaviCommand()
    {
        $this->artisan('install:navi')
            ->expectsOutput('Running Install Navi Command ...')
            ->expectsOutput('Creating BlockHtml ...')
            ->expectsOutput('Creating BlockEntity ...');

//        foreach (NaviHtmlBlocks::BLOCKS as $block) {
//            $this->assertDatabaseHas('block_html', [
//                'module'       => 'Navi',
//                'blade'        => $block['blade'],
//                'instructions' => $block['instructions'],
//                'settings'     => serialize($block['values']),
//                'layout'       => serialize($block['layout']),
//            ]);
//        }
//
//        foreach (NaviEntityBlocks::BLOCKS as $block) {
//            $this->assertDatabaseHas('block_entities', [
//                'module'       => 'Navi',
//                'blade'        => $block['blade'],
//                'entity'       => $block['entity'],
//                'instructions' => $block['instructions'],
//                'settings'     => serialize($block['values']),
//                'layout'       => serialize($block['layout']),
//            ]);
//        }
    }
}
